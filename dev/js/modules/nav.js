var base = base || {};

base.nav = (function (window, $) {
    'use strict';

    var hijackInternalLink = function (e) {
        e.preventDefault();
        e.stopPropagation();
        // Check the link is not to the current page
        if (this.pathname !== window.location.pathname) {
            base.page.go(this.pathname);
        }
    };

    $('html').on('click', 'a[href^="/"]', hijackInternalLink);

})(window, window.jQuery);
